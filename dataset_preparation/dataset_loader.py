import os
import shutil
import csv
import json
import xml.etree.ElementTree as ET
import urllib.request
import datetime

from sklearn.model_selection import train_test_split

class DatasetLoader:
    def __init__(self):
        self.images_folder = 'images'
        self.dst_dataset_folder = None
        self.samples = list()
        self.col2ind = None
        self.name2id = dict()

    @staticmethod
    def _check_data_folder_for_validity(folder_path):
        if not os.path.isdir(folder_path):
            raise ValueError('descr folder path is not correct')
        if len(os.listdir(folder_path)) == 0:
            raise ValueError("descr folder path can't be empty")

    @staticmethod
    def download_img_by_url(url, dst_path):
        print('Downloading: ' + url)
        urllib.request.urlretrieve(url, dst_path)

    def _get_samples_from_csv_file(self, csv_file_path):
        with open(csv_file_path, newline='') as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            csv_rows = [row for row in csv_reader]
            col2ind = {column_name.strip('"').strip("'"): id_column_name for id_column_name, column_name in
                       enumerate(csv_rows[0])}
            if self.col2ind == None:
                self.col2ind = col2ind
            else:
                if col2ind != self.col2ind:
                    raise ValueError('Check format XML files')
            del csv_rows[0]
            return csv_rows

    def _parse_xml_in_samples(self):
        counter_object_idxs = 0
        for idx_sample, sample in enumerate(self.samples):
            sample_dict = dict()
            filename = (sample[self.col2ind['Файлы']]).strip('/n')
            filepath = os.path.join(self.images_folder, filename)
            xml_string = sample[self.col2ind['XML']]
            xml_root = ET.ElementTree(ET.fromstring(xml_string))
            annotation = xml_root.find('annotation')
            annotation = xml_root if annotation == None else annotation
            url = annotation.find('filename').text
            if not os.path.isfile(filepath):
                self.download_img_by_url(url, filepath)
            imagesize = annotation.find('imagesize')
            height = int(float(imagesize.find('nrows').text))
            width = int(float(imagesize.find('ncols').text))
            sample_dict['license'] = 1
            sample_dict['file_name'] = filename
            sample_dict['coco_url'] = 'http://nothing.com/nothing.jpg'
            sample_dict['height'] = height
            sample_dict['width'] = width
            sample_dict['date_captured'] = datetime.datetime.utcnow().isoformat(' ')
            sample_dict["flickr_url"] = 'http://nothing.com/nothing.jpg'
            sample_dict['id'] = idx_sample
            sample_dict['annotation'] = list()

            objects = annotation.findall('{http://www.w3.org/1999/xhtml}object')
            for idx_object, object in enumerate(objects):
                object_dict = dict()
                name = object.find('{http://www.w3.org/1999/xhtml}name').text
                if name not in self.name2id:
                    self.name2id[name] = len(self.name2id)
                deleted = object.find('{http://www.w3.org/1999/xhtml}deleted').text
                verified = object.find('{http://www.w3.org/1999/xhtml}verified').text
                occluded = object.find('{http://www.w3.org/1999/xhtml}occluded').text
                id = object.find('{http://www.w3.org/1999/xhtml}id').text
                type = object.find('{http://www.w3.org/1999/xhtml}type').text
                polygon = object.find('{http://www.w3.org/1999/xhtml}polygon')
                username = polygon.find('{http://www.w3.org/1999/xhtml}username').text
                pts = polygon.findall('{http://www.w3.org/1999/xhtml}pt')
                x_list = list()
                y_list = list()
                for pt in pts:
                    x = pt.find('{http://www.w3.org/1999/xhtml}x').text
                    y = pt.find('{http://www.w3.org/1999/xhtml}y').text
                    x_list.append(float(x))
                    y_list.append(float(y))
                x_min = min(x_list)
                x_max = max(x_list)
                obj_width = x_max - x_min
                y_min = min(y_list)
                y_max = max(y_list)
                obj_height = y_max - y_min
                area = obj_width * obj_width
                bbox = [x_min, y_min, obj_width, obj_height]
                segmentation = [[x_min, y_min, x_max, y_min, x_max, y_max, x_min, y_max]]
                if x_min < 0 or y_min < 0:
                    continue
                if x_max > (width - 1) or y_max > (height - 1):
                    continue
                if obj_width <= 3 or obj_height <= 3:
                    continue
                object_dict['segmentation'] = segmentation
                object_dict['area'] = area
                object_dict['iscrowd'] = 0
                object_dict['image_id'] = idx_sample
                object_dict['bbox'] = bbox
                object_dict['category_id'] = self.name2id[name]
                object_dict['id'] = counter_object_idxs
                sample_dict['annotation'].append(object_dict)
                counter_object_idxs += 1
            self.samples[idx_sample] = sample_dict

    def _make_coco_dataset(self, samples, dataset_name):
        os.makedirs(os.path.join(self.dst_dataset_folder, 'annotations'), exist_ok=True)
        os.makedirs(os.path.join(self.dst_dataset_folder, dataset_name), exist_ok=True)
        coco_dataset_dict = dict()
        coco_dataset_dict['info'] = {
            "description": "test_task_gr_mri_spine_" + str(dataset_name),
            "url": "https://github.com/someone/some_project",
            "version": "0.1.0",
            "year": 2019,
            "contributor": "aWin",
            "date_created": datetime.datetime.utcnow().isoformat(' ')
        }
        coco_dataset_dict['licenses'] = [
            {
                "id": 1,
                "name": "Free License",
                "url": "https://freelicense.com"
            }
        ]
        coco_dataset_dict['categories'] = list()
        for category_name, category_id in self.name2id.items():
            category = dict()
            category['supercategory'] = 'mri_object'
            category['id'] = int(category_id)
            category['name'] = category_name
            coco_dataset_dict['categories'].append(category)
        coco_dataset_dict['images'] = list()
        coco_dataset_dict['annotations'] = list()
        for sample in samples:
            image = dict()
            image['license'] = sample['license']
            image['file_name'] = sample['file_name']
            shutil.copyfile(os.path.join(self.images_folder, sample['file_name']),
                            os.path.join(self.dst_dataset_folder, dataset_name, sample['file_name']))
            image['coco_url'] = sample['coco_url']
            image['height'] = sample['height']
            image['width'] = sample['width']
            image['date_captured'] = sample['date_captured']
            image['flickr_url'] = sample['flickr_url']
            image['id'] = sample['id']
            coco_dataset_dict['images'].append(image)
            coco_dataset_dict['annotations'] += sample['annotation']
        with open(os.path.join(self.dst_dataset_folder, 'annotations', dataset_name + '.json'), 'w') as f:
            json.dump(coco_dataset_dict, f)

    def load_samples_from_folder_with_csv(self, descr_folder, excluding_unmarked_images=True):
        self._check_data_folder_for_validity(descr_folder)
        files_names_list = os.listdir(descr_folder)
        csv_files_names_list = sorted([file_name for file_name in files_names_list if file_name[-4:] == '.csv'])
        if len(csv_files_names_list) == 0:
            raise ValueError("There are no csv-files in descr folder")
        for csv_file_name in csv_files_names_list:
            self.samples += self._get_samples_from_csv_file(os.path.join(descr_folder, csv_file_name))
            self.samples = sorted(self.samples, key=lambda sample: sample[self.col2ind['Файлы']])
        if excluding_unmarked_images:
            self.samples = [sample for sample in self.samples if 'xmlns' in sample[self.col2ind['XML']]]
        # parse XMLs
        self._parse_xml_in_samples()

    def make_train_and_val_coco_datasets(self, dst_dataset_folder='dataset'):
        self.dst_dataset_folder = dst_dataset_folder
        if os.path.isdir(dst_dataset_folder):
            shutil.rmtree(dst_dataset_folder)
        os.makedirs(dst_dataset_folder)

        samples_train, samples_val = train_test_split(self.samples, test_size=0.2, shuffle=False)
        self._make_coco_dataset(samples_train, 'train_gr_mri_spine')
        self._make_coco_dataset(samples_val, 'val_gr_mri_spine')


if __name__ == '__main__':
    os.chdir('dataset_preparation/')
    dataset_loader = DatasetLoader()
    dataset_loader.load_samples_from_folder_with_csv(descr_folder='descr')
    dataset_loader.make_train_and_val_coco_datasets()
    os.chdir('../')
    os.makedirs('data', exist_ok=True)
    os.symlink('dataset_preparation/dataset', 'data/dataset')

